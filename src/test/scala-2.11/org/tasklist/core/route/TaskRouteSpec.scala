package org.tasklist.core.route

import java.util.UUID

import akka.actor.Actor.Receive
import akka.actor.{Actor, Props}
import org.scalatest.{Matchers, WordSpec}
import org.tasklist.core.presentation.TaskJsonProtocol
import org.tasklist.domain.TaskWriteModel.CreateTask
import org.tasklist.domain._
import spray.http.StatusCodes
import spray.http.ContentTypes._
import spray.http.HttpHeaders._
import spray.httpx.SprayJsonSupport
import spray.json.{JsArray, JsString, JsObject}
import spray.testkit.ScalatestRouteTest


class TaskRouteSpec extends WordSpec with ScalatestRouteTest with Matchers {

  import SprayJsonSupport._
  import TaskJsonProtocol._

  val r = system.actorOf(Props(new Actor {
    def receive: Receive = { case _ => }
  }))
  val w = system.actorOf(Props(new Actor {
    def receive: Receive = { case _ => }
  }))

  val controller = new TaskRoute
  val route = controller.definition(w, r)

  "The service" should {

    "return a task list it it's serialized version" in {
      Get("/task") ~> route ~> check {
        status == StatusCodes.OK
        val list = responseAs[List[Task]]
        list match {
          case task :: Nil => task.description.title shouldEqual "test"
          case _ => fail("Expected one element list containing task.")
        }
      }
    }

    "create new task in response to create new task command" in {
      Post("/task", CreateTask(Description("test", "desc of test", List()), TaskPriority.Low)) ~> route ~> check {
        status == StatusCodes.OK
        val task = responseAs[Task]
        task.priority shouldEqual TaskPriority.Low

      }
    }

    "allow retriving single task by it's UUID" in {
      Get("/task/" + UUID.randomUUID()) ~> route ~> check {
        status == StatusCodes.OK
      }
    }

    "allow updating task description" in {
      val entity = JsObject(
        "name" ->  JsString("description"),
        "content" -> JsObject(
          "title" -> JsString("test"),
          "title" -> JsString("test of description"),
          "labels" -> JsArray.empty
        )
      )

      Put("/task/" + UUID.randomUUID() + "/description", entity) ~> route ~> check {
        status == StatusCodes.OK

      }
    }
  }
}
