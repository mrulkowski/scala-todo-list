package org.tasklist.domain

import java.util.UUID

object TaskPriority {
  sealed trait Value {}

  case object Low extends Value
  case object Normal extends Value
  case object High extends Value
}

object TaskStatus {
  sealed trait Value {}

  case object NotStarted extends Value
  case object InProgress extends Value
  case object Resolved extends Value
}

case class Description (title: String, content: String, labels: List[String])

case class Task(id: UUID, description: Description, status: TaskStatus.Value, priority: TaskPriority.Value)
