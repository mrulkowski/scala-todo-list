package org.tasklist.domain

import java.util.UUID

import akka.actor.{Props, Actor, ActorLogging}

object TaskWriteModel {

  case class CreateTask(description: Description, priority: TaskPriority.Value)

  case class UpdateTaskStatus(status: TaskStatus.Value)

  case class UpdateTaskPriority(priority: TaskPriority.Value)

  case class UpdateTaskDescription(description: Description)

  case class RemoveTask()

  def props = Props(new TaskWriteModel)
}


class TaskWriteModel extends Actor with ActorLogging {

  import TaskWriteModel._

  // TODO - check if we could use state monad to share state between read and write models (at least temporally)
  var inMemoryStorage = Map[UUID, Task]()

  def receive = {
    case (CreateTask(desc, priority)) =>
      val uuid: UUID = UUID.randomUUID()
      val task = new Task(uuid, desc, TaskStatus.NotStarted, priority)

      inMemoryStorage = inMemoryStorage + (uuid -> task)
    case (id: UUID, UpdateTaskStatus(status)) =>

  }
}
