package org.tasklist.domain

import akka.actor.{Props, ActorLogging, Actor}


object TaskReadModel {
  case class GetAllTasks()
  case class GetTask()

  def props = Props(new TaskReadModel)
}

class TaskReadModel extends Actor with ActorLogging {

  def receive = {
    case "" =>
  }
}
