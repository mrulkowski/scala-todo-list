package org.tasklist.core.route

import java.util.UUID

import akka.actor.ActorRef
import org.tasklist.domain.TaskWriteModel.CreateTask
import org.tasklist.domain.{TaskPriority, Description, TaskStatus, Task}
import spray.httpx.SprayJsonSupport
import spray.routing.{Directives, Route}
import org.tasklist.core.presentation.TaskJsonProtocol

class TaskRoute extends Directives {

  import SprayJsonSupport._
  import TaskJsonProtocol._

  def definition(writer: ActorRef, reader: ActorRef): Route =
    pathPrefix("task") {
      pathPrefix(JavaUUID) { uuid =>
        pathEndOrSingleSlash {
          get {
            complete {
              "GET - single"
            }
          }
        } ~
        path("description") {
          put {
            complete {
              "PUT - description"
            }
          }
        } ~
        path("status") {
          put {
            complete {
              "PUT - description"
            }
          }
        } ~
        path("priority") {
          put {
            complete {
              "PUT - description"
            }
          }
        }
      } ~
      pathEndOrSingleSlash {
        get {
          complete {
            List(
              new Task(UUID.randomUUID(), Description("test", "", List()), TaskStatus.NotStarted, TaskPriority.Normal)
            )
          }
        } ~
        post {
          entity(as[CreateTask]) { createTask =>
            complete {
              new Task(UUID.randomUUID(), createTask.description, TaskStatus.NotStarted, createTask.priority)
            }
          }
        }
      }
    }

}
