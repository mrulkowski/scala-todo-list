package org.tasklist.core

import akka.actor.Props
import spray.routing.{Route, HttpServiceActor}

object HttpListener {
  def props(route: Route) = Props(new HttpListener(route))
}

class HttpListener(route: Route) extends HttpServiceActor {
  override def receive: Receive = runRoute(route)
}
