package org.tasklist.core

import akka.actor.ActorSystem
import akka.io.IO
import akka.util.Timeout
import akka.pattern.ask
import org.tasklist.core.route.TaskRoute
import org.tasklist.domain.{TaskReadModel, TaskWriteModel}
import spray.can.Http

import scala.concurrent.duration._

object Boot extends App {


  implicit val actorSystem = ActorSystem("task-list")

  // TODO - instead of class use function that will generate routes (?)
  val controller = new TaskRoute

  // TODO - reduce list of all routes by combining them or in one of components of this application
  val taskWriter = actorSystem.actorOf(TaskWriteModel.props, "task-writer")
  val taskReader = actorSystem.actorOf(TaskReadModel.props, "task-reader")
  val routes = controller.definition(taskWriter, taskReader)

  val listener = actorSystem.actorOf(HttpListener.props(routes), "http-listener")

  implicit val timeout = Timeout(5.seconds)
  IO(Http) ? Http.Bind(listener, interface = "localhost", port = 8080)
}
