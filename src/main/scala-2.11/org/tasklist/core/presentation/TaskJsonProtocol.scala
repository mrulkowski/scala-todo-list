package org.tasklist.core.presentation

import org.tasklist.domain.TaskWriteModel.{UpdateTaskDescription, CreateTask}
import org.tasklist.domain.{Task, Description, TaskPriority, TaskStatus}
import spray.json._

object TaskJsonProtocol extends DefaultJsonProtocol {

  import SharedJsonFormats._

  implicit object TaskStatusFormat extends JsonFormat[TaskStatus.Value] {
    override def write(obj: TaskStatus.Value): JsValue = obj match {
      case TaskStatus.NotStarted => JsNumber(0)
      case TaskStatus.InProgress => JsNumber(1)
      case TaskStatus.Resolved => JsNumber(2)
    }

    override def read(json: JsValue): TaskStatus.Value = json match {
      case JsNumber(value) => value.toInt match {
        case 0 => TaskStatus.NotStarted
        case 1 => TaskStatus.InProgress
        case 2 => TaskStatus.Resolved
        case _ => throw new DeserializationException("Task status is resolved only to numbers (1, 2, 3), but received: " + value)
      }
      case _ => throw new DeserializationException("Task status is represented by number, received: " + json)
    }
  }

  implicit object TaskPriorityFormat extends JsonFormat[TaskPriority.Value] {
    override def write(obj: TaskPriority.Value): JsValue = obj match {
      case TaskPriority.Low => JsString("low")
      case TaskPriority.Normal => JsString("normal")
      case TaskPriority.High => JsString("high")
    }

    override def read(json: JsValue): TaskPriority.Value = json match {
      case JsString(repres) => repres match {
        case "low" => TaskPriority.Low
        case "normal" => TaskPriority.Normal
        case "high" => TaskPriority.High
        case _ => throw new DeserializationException("Task status is resolved only to codes: (low, normal, high), but received: " + repres)
      }
      case _ => throw new DeserializationException("Task priority is represented by string, received: " + json)
    }
  }

  implicit val taskDescriptionFormat = jsonFormat3(Description)
  implicit val taskFormat = jsonFormat4(Task.apply)

  // ==== Commands Formats =====

  implicit val taskCreateFormat = jsonFormat2(CreateTask)
  implicit val taskUpdateDescriptionFormat = jsonFormat1(UpdateTaskDescription)

}
