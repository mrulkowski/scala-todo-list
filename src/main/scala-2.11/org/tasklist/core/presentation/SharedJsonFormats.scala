package org.tasklist.core.presentation

import java.util.UUID
import spray.json._

object SharedJsonFormats {

  implicit object JavaUUID extends RootJsonFormat[UUID] {
    override def read(json: JsValue): UUID = json match {
      case JsString(x) => UUID.fromString(x)
      case _ => throw new DeserializationException("Expected UUID, but found: " + json)
    }

    override def write(obj: UUID): JsValue = JsString(obj.toString)
  }


}
